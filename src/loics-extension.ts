import { interval } from 'rxjs'
import { map } from 'rxjs/operators'
import * as sourcegraph from 'sourcegraph'

export function activate(ctx: sourcegraph.ExtensionContext): void {
   ctx.subscriptions.add(
    sourcegraph.languages.registerReferenceProvider(['*'], {
        provideReferences: () => {
            throw new Error('Error providing references')
        }
    })
   )
}

// Sourcegraph extension documentation: https://docs.sourcegraph.com/extensions/authoring
